package cli

import (
	"fmt"
	"github.com/cosmos/cosmos-sdk/client/context"
	"github.com/cosmos/cosmos-sdk/codec"
	"github.com/spf13/cobra"
)

func GetCmdFact(queryRoute string, cdc *codec.Codec) *cobra.Command {
	return &cobra.Command{
		Use:   "get-fact [fact-address]",
		Short: "get facts by address",
		Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			cliCtx := context.NewCLIContext().WithCodec(cdc)
			address := args[0]
			res, err := cliCtx.QueryWithData(fmt.Sprintf("custom/%s/fact/%s", queryRoute, address), nil)
			if err != nil {
				fmt.Printf("could not resolve address - %s \n", string(address))
				return nil
			}

			return cliCtx.PrintOutput(factOutput{string(res)})
		},
	}
}

type factOutput struct {
	Value string `json:"value"`
}

func (r factOutput) String() string {
	return r.Value
}

func GetCmdFactDimension(queryRoute string, cdc *codec.Codec) *cobra.Command {
	return &cobra.Command{
		Use:   "get-dimension [dimension-name]",
		Short: "get details on a fact dimension",
		Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			cliCtx := context.NewCLIContext().WithCodec(cdc)
			dimensionName := args[0]
			res, err := cliCtx.QueryWithData(fmt.Sprintf("custom/%s/fact-dimension/%s", queryRoute, dimensionName), nil)
			if err != nil {
				fmt.Printf("could not resolve dimension name - %s \n", string(dimensionName))
				return nil
			}

			return cliCtx.PrintOutput(factDimensionOutput{string(res)})
		},
	}
}

type factDimensionOutput struct {
	Value string `json:"value"`
}

func (r factDimensionOutput) String() string {
	return r.Value
}

func GetCmdFactsByDimension(queryRoute string, cdc *codec.Codec) *cobra.Command {
	return &cobra.Command{
		Use:   "get-facts-by-dimension [dimension-name]",
		Short: "get facts by dimension",
		Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			cliCtx := context.NewCLIContext().WithCodec(cdc)
			dimensionName := args[0]
			res, err := cliCtx.QueryWithData(fmt.Sprintf("custom/%s/fact-by-dimension/%s", queryRoute, dimensionName), nil)
			if err != nil {
				fmt.Printf("could not resolve dimension name - %s \n", string(dimensionName))
				return nil
			}

			return cliCtx.PrintOutput(factsByDimensionOutput{string(res)})
		},
	}
}

type factsByDimensionOutput struct {
	Value string `json:"value"`
}

func (r factsByDimensionOutput) String() string {
	return r.Value
}
