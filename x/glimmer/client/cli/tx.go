package cli

import (
	"github.com/cosmos/cosmos-sdk/client/context"
	"github.com/cosmos/cosmos-sdk/client/utils"
	"github.com/cosmos/cosmos-sdk/codec"
	sdk "github.com/cosmos/cosmos-sdk/types"
	authTxb "github.com/cosmos/cosmos-sdk/x/auth/client/txbuilder"
	"github.com/spf13/cobra"
	"gitlab.com/glimmer-network/glimmer-node.git/x/glimmer"
)

/***********************************************************************s
	Add Fact
***********************************************************************/

func GetCmdAddFact(cdc *codec.Codec) *cobra.Command {
	return &cobra.Command{
		Use:   "add-fact [address] [fact-dimension] [fact-type] [response-type] [response-text] --from [address]",
		Short: "broadcast a new fact to the Glimmer blockchain",
		Args:  cobra.ExactArgs(5),
		RunE: func(cmd *cobra.Command, args []string) error {
			cliCtx := context.NewCLIContext().WithCodec(cdc).WithAccountDecoder(cdc)
			txBulder := authTxb.NewTxBuilderFromCLI().WithTxEncoder(utils.GetTxEncoder(cdc))

			addrTo, err := sdk.AccAddressFromBech32(args[0])
			if err != nil {
				return err
			}

			msg := glimmer.NewMsgAddFact(addrTo, args[1], args[2], args[3], args[4])
			err = msg.ValidateBasic()
			if err != nil {
				return err
			}

			cliCtx.PrintResponse = true
			return utils.CompleteAndBroadcastTxCLI(txBulder, cliCtx, []sdk.Msg{msg})
		},
	}
}

/***********************************************************************s
	Add Dimension
***********************************************************************/

func GetCmdAddFactDimension(cdc *codec.Codec) *cobra.Command {
	return &cobra.Command{
		Use:   "add-fact-dimension [name] [isSensitive 1|0)] [description] [asQuestion] [asProlog] --from [address]",
		Short: "broadcast a new fact to the Glimmer blockchain",
		Args:  cobra.ExactArgs(5),
		RunE: func(cmd *cobra.Command, args []string) error {
			cliCtx := context.NewCLIContext().WithCodec(cdc).WithAccountDecoder(cdc)
			txBuilder := authTxb.NewTxBuilderFromCLI().WithTxEncoder(utils.GetTxEncoder(cdc))
			isSensitive := args[1] == "1"

			msg := glimmer.NewMsgAddFactDimension(args[0], isSensitive, args[2], args[3], args[4], cliCtx.GetFromAddress())

			err := msg.ValidateBasic()
			if err != nil {
				return err
			}

			cliCtx.PrintResponse = true
			return utils.CompleteAndBroadcastTxCLI(txBuilder, cliCtx, []sdk.Msg{msg})
		},
	}
}

/***********************************************************************s
	Send coins
***********************************************************************/

func GetCmdSendCoins(cdc *codec.Codec) *cobra.Command {
	return &cobra.Command{
		Use:   "send-coins [toAddress] [amount]",
		Short: "send coins to an address",
		Args:  cobra.ExactArgs(2),
		RunE: func(cmd *cobra.Command, args []string) error {
			cliCtx := context.NewCLIContext().WithCodec(cdc).WithAccountDecoder(cdc)
			txBldr := authTxb.NewTxBuilderFromCLI().WithTxEncoder(utils.GetTxEncoder(cdc))

			if err := cliCtx.EnsureAccountExists(); err != nil {
				return err
			}

			addrTo, err := sdk.AccAddressFromBech32(args[0])
			if err != nil {
				return err
			}

			coins, err := sdk.ParseCoins(args[1])
			if err != nil {
				return err
			}

			msg := glimmer.NewMsgSendCoins(cliCtx.GetFromAddress(), addrTo, coins)
			err = msg.ValidateBasic()
			if err != nil {
				return err
			}

			cliCtx.PrintResponse = true

			return utils.CompleteAndBroadcastTxCLI(txBldr, cliCtx, []sdk.Msg{msg})
		},
	}
}