package client

import (
	"github.com/cosmos/cosmos-sdk/client"
	"github.com/spf13/cobra"
	amino "github.com/tendermint/go-amino"
	glimmercmd "gitlab.com/glimmer-network/glimmer-node.git/x/glimmer/client/cli"
)

// ModuleClient exports all client functionality from this module
type ModuleClient struct {
	storeKey string
	cdc      *amino.Codec
}

func NewModuleClient(storeKey string, cdc *amino.Codec) ModuleClient {
	return ModuleClient{storeKey, cdc}
}

// GetQueryCmd returns the cli query commands for this module
func (mc ModuleClient) GetQueryCmd() *cobra.Command {

	govQueryCmd := &cobra.Command{
		Use:   "glimmer",
		Short: "Querying commands for the glimmer module",
	}

	govQueryCmd.AddCommand(client.GetCommands(
		glimmercmd.GetCmdFactByDimension(mc.storeKey, mc.cdc),
	)...)

	return govQueryCmd
}

// GetTxCmd returns the transaction commands for this module
func (mc ModuleClient) GetTxCmd() *cobra.Command {
	govTxCmd := &cobra.Command{
		Use:   "glimmer",
		Short: "glimmer transactions subcommands",
	}

	govTxCmd.AddCommand(client.PostCommands(
		glimmercmd.GetCmdAddFact(mc.cdc),
		glimmercmd.GetCmdAddFactDimension(mc.cdc),
		glimmercmd.GetCmdSendCoins(mc.cdc),
	)...)

	return govTxCmd
}
