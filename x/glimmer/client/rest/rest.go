package rest

import (
	"fmt"
	"github.com/cosmos/cosmos-sdk/client/context"
	"github.com/cosmos/cosmos-sdk/client/rest"
	"github.com/cosmos/cosmos-sdk/codec"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/gorilla/mux"
	"gitlab.com/glimmer-network/glimmer-node.git/x/glimmer"
	"net/http"
)

const (
	factAddress = "factAddress"
)

const (
	factDimensionName = "factAddress"
)

func RegisterRoutes(cliCtx context.CLIContext, r *mux.Router, cdc *codec.Codec, storeName string) {
	r.HandleFunc(fmt.Sprintf("/%s/send", storeName), sendCoinsHandler(cdc, cliCtx)).Methods("POST")
	r.HandleFunc(fmt.Sprintf("/%s/fact", storeName), addFactHandler(cdc, cliCtx)).Methods("POST")
	r.HandleFunc(fmt.Sprintf("/%s/fact-dimension", storeName), addFactDimensionHandler(cdc, cliCtx)).Methods("POST")
	r.HandleFunc(fmt.Sprintf("/%s/fact/{%s}", storeName, factAddress), getFactHandler(cdc, cliCtx, storeName)).Methods("GET")
	r.HandleFunc(fmt.Sprintf("/%s/fact-dimension/{%s}", storeName, factDimensionName), getDimensionHandler(cdc, cliCtx, storeName)).Methods("GET")
	r.HandleFunc(fmt.Sprintf("/%s/facts-by-dimension/{%s}", storeName, factDimensionName), getFactsByDimensionHandler(cdc, cliCtx, storeName)).Methods("GET")
}

/***********************************************************************
	Get fact by address (GET)
***********************************************************************/

func getFactHandler(cdc *codec.Codec, cliCtx context.CLIContext, storeName string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		paramType := vars[factAddress]

		res, err := cliCtx.QueryWithData(fmt.Sprintf("custom/%s/fact/%s", storeName, paramType), nil)
		if err != nil {
			rest.WriteErrorResponse(w, http.StatusNotFound, err.Error())
			return
		}

		rest.PostProcessResponse(w, cdc, res, cliCtx.Indent)
	}
}

/***********************************************************************
	Get fact dimension by address (GET)
***********************************************************************/

func getDimensionHandler(cdc *codec.Codec, cliCtx context.CLIContext, storeName string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		paramType := vars[factAddress]

		res, err := cliCtx.QueryWithData(fmt.Sprintf("custom/%s/fact-dimension/%s", storeName, paramType), nil)
		if err != nil {
			rest.WriteErrorResponse(w, http.StatusNotFound, err.Error())
			return
		}

		rest.PostProcessResponse(w, cdc, res, cliCtx.Indent)
	}
}

/***********************************************************************
	Get facts by dimension (GET)
***********************************************************************/

func getFactsByDimensionHandler(cdc *codec.Codec, cliCtx context.CLIContext, storeName string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		paramType := vars[factAddress]

		res, err := cliCtx.QueryWithData(fmt.Sprintf("custom/%s/facts-by-dimension/%s", storeName, paramType), nil)
		if err != nil {
			rest.WriteErrorResponse(w, http.StatusNotFound, err.Error())
			return
		}

		rest.PostProcessResponse(w, cdc, res, cliCtx.Indent)
	}
}

/***********************************************************************
	Add fact (POST)
***********************************************************************/

type addFactReq struct {
	BaseReq rest.BaseReq `json:"base_req"`
	Address      string    `json:"address"`
	Dimension    string    `json:"dimension"`
	FactType     string    `json:"factType"`
	ResponseType string    `json:"responseType"`
	Response     string    `json:"response"`
}

func addFactHandler(cdc *codec.Codec, cliCtx context.CLIContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req addFactReq

		if !rest.ReadRESTReq(w, r, cdc, &req) {
			rest.WriteErrorResponse(w, http.StatusBadRequest, "failed to parse request")
			return
		}

		baseReq := req.BaseReq.Sanitize()
		if !baseReq.ValidateBasic(w) {
			return
		}

		factAddress, err := sdk.AccAddressFromBech32(req.Address)
		if err != nil {
			rest.WriteErrorResponse(w, http.StatusBadRequest, err.Error())
			return
		}

		// create the message
		msg := glimmer.NewMsgAddFact(factAddress, req.Dimension, req.FactType, req.ResponseType, req.Response)
		err = msg.ValidateBasic()
		if err != nil {
			rest.WriteErrorResponse(w, http.StatusBadRequest, err.Error())
			return
		}

		rest.CompleteAndBroadcastTxREST(w, r, cliCtx, baseReq, []sdk.Msg{msg}, cdc)
	}
}

/***********************************************************************
	Add fact dimension (POST)
***********************************************************************/

type addFactDimensionReq struct {
	BaseReq     rest.BaseReq `json:"base_req"`
	Name        string       `json:"name"`
	IsSensitive string       `json:"isSensitive"`
	Description string       `json:"description"`
	AsQuestion  string       `json:"asQuestion"`
	AsProlog    string       `json:"asProlog"`
	From        string       `json:"from"`
}

func addFactDimensionHandler(cdc *codec.Codec, cliCtx context.CLIContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req addFactDimensionReq

		if !rest.ReadRESTReq(w, r, cdc, &req) {
			rest.WriteErrorResponse(w, http.StatusBadRequest, "failed to parse request")
			return
		}

		baseReq := req.BaseReq.Sanitize()
		if !baseReq.ValidateBasic(w) {
			return
		}

		fromAddress, err := sdk.AccAddressFromBech32(req.From)
		if err != nil {
			rest.WriteErrorResponse(w, http.StatusBadRequest, err.Error())
			return
		}

		msg := glimmer.NewMsgAddFactDimension(req.Name, req.IsSensitive == "1", req.Description, req.AsQuestion, req.AsProlog, fromAddress)
		err = msg.ValidateBasic()
		if err != nil {
			rest.WriteErrorResponse(w, http.StatusBadRequest, err.Error())
			return
		}

		rest.CompleteAndBroadcastTxREST(w, r, cliCtx, baseReq, []sdk.Msg{msg}, cdc)
	}
}

/***********************************************************************
	Send coins (POST)
***********************************************************************/

type sendCoinsReq struct {
	BaseReq rest.BaseReq `json:"base_req"`
	To      string       `json:"to"`
	From    string       `json:"from"`
	Amount  string       `json:"amount"`
}

func sendCoinsHandler(cdc *codec.Codec, cliCtx context.CLIContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req sendCoinsReq

		if !rest.ReadRESTReq(w, r, cdc, &req) {
			rest.WriteErrorResponse(w, http.StatusBadRequest, "failed to parse request")
			return
		}

		baseReq := req.BaseReq.Sanitize()
		if !baseReq.ValidateBasic(w) {
			return
		}

		addrTo, err := sdk.AccAddressFromBech32(req.To)
		if err != nil {
			rest.WriteErrorResponse(w, http.StatusBadRequest, err.Error())
			return
		}

		addrFrom, err := sdk.AccAddressFromBech32(req.From)
		if err != nil {
			rest.WriteErrorResponse(w, http.StatusBadRequest, err.Error())
			return
		}

		coins, err := sdk.ParseCoins(req.Amount)
		if err != nil {
			rest.WriteErrorResponse(w, http.StatusBadRequest, err.Error())
			return
		}

		// create the message
		msg := glimmer.NewMsgSendCoins(addrFrom, addrTo, coins)
		err = msg.ValidateBasic()
		if err != nil {
			rest.WriteErrorResponse(w, http.StatusBadRequest, err.Error())
			return
		}

		rest.CompleteAndBroadcastTxREST(w, r, cliCtx, baseReq, []sdk.Msg{msg}, cdc)
	}
}