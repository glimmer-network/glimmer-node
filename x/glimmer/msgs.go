package glimmer

import (
	"encoding/json"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"strconv"
	"time"
)

/***********************************************************************
	MsgAddFact message type : adds a new fact dimension
***********************************************************************/

// ToDo: some sort of union case for FactType?
type MsgAddFact struct {
	Address      sdk.AccAddress
	Dimension    string
	FactType     string
	ResponseType string
	Response     string
}

func NewMsgAddFact(address sdk.AccAddress, dimension string, factType string, response string, responseType string) MsgAddFact {
	return MsgAddFact{
		Address      : address      ,
		Dimension    : dimension    ,
		FactType     : factType     ,
		ResponseType : responseType ,
		Response     : response     ,
	}
}

func (msg MsgAddFact) Route() string { return "glimmer" }

func (msg MsgAddFact) Type() string { return "add_fact" }

func (msg MsgAddFact) ValidateBasic() sdk.Error {

	if msg.Address.Empty() {
		return sdk.ErrInvalidAddress(msg.Address.String())
	}

	if len (msg.Dimension ) == 0 {
		return sdk.ErrUnknownRequest("Dimension must be specified")
	}

	if msg.FactType != "private" && msg.FactType != "public" {
		return sdk.ErrUnknownRequest("FactType can be either 'public' or 'private'")
	}

	if msg.ResponseType != "int" && msg.ResponseType != "date-rfc3339" && msg.ResponseType != "bool" && msg.ResponseType != "float"   {
		return sdk.ErrUnknownRequest("ResponseType can be one of [ 'int' ; 'date-rfc3339' ; 'bool' ; 'float' ] ")
	}

	if len (msg.Response) == 0 {
		return sdk.ErrUnknownRequest("Response must be not be empty")
	}

	switch msg.ResponseType {
		case "int":
			_ , err := strconv.Atoi(msg.Response)
			if err != nil {
				return sdk.ErrUnknownRequest("ResponseType was 'int' but Response does not appear to match.")
			}
		case "bool":
			boolAsInt, err := strconv.Atoi(msg.Response)
			if err != nil || (boolAsInt != 1 && boolAsInt != 0 ) {
				return sdk.ErrUnknownRequest("ResponseType was 'bool' but Response was neither 0 or 1.")
			}
		case "date-rfc3339":
			_, err := time.Parse(time.RFC3339, msg.Response)
			if err != nil {
				return sdk.ErrUnknownRequest("ResponseType was 'date-rfc3339' but couldn't parse Response.")
			}
		case "float":
			_, err := strconv.ParseFloat(msg.Response, 64)
			if err != nil {
				return sdk.ErrUnknownRequest("ResponseType was 'float' but couldn't parse Response.")
			}
	}

	return nil
}

func (msg MsgAddFact) GetSignBytes() []byte {
	b, err := json.Marshal(msg)
	if err != nil {
		panic(err)
	}
	return sdk.MustSortJSON(b)
}

func (msg MsgAddFact) GetSigners() []sdk.AccAddress {
	return []sdk.AccAddress{msg.Address}
}

/***********************************************************************
	MsgAddFactDimension message type : adds a new fact dimension
***********************************************************************/

type MsgAddFactDimension struct {
	Name        string
	IsSensitive bool
	Description string
	AsQuestion  string
	AsProlog    string
	From        sdk.AccAddress
}

func NewMsgAddFactDimension(name string, isSensitive bool, description string, asQuestion string, asProlog string, from sdk.AccAddress) MsgAddFactDimension {
	return MsgAddFactDimension{
		Name        : name        ,
		IsSensitive : isSensitive ,
		Description : description ,
		AsQuestion  : asQuestion  ,
		AsProlog    : asProlog    ,
		From        : from        ,
	}
}

func (msg MsgAddFactDimension) Route() string { return "glimmer" }

func (msg MsgAddFactDimension) Type() string { return "add_fact_dimension" }

func (msg MsgAddFactDimension) ValidateBasic() sdk.Error {

	// Todo: need to check for invalid characters
	if len(msg.Name) == 0 {
			return sdk.ErrUnknownRequest("Dimension name cannot be empty")
	}

	return nil
}

func (msg MsgAddFactDimension) GetSignBytes() []byte {
	b, err := json.Marshal(msg)
	if err != nil {
		panic(err)
	}
	return sdk.MustSortJSON(b)
}

func (msg MsgAddFactDimension) GetSigners() []sdk.AccAddress {
	return []sdk.AccAddress{msg.From}
}

/***********************************************************************
	MsgSendCoins message type : sending coins from one party to another
***********************************************************************/

type MsgSendCoins struct {
	From   sdk.AccAddress
	To     sdk.AccAddress
	Amount sdk.Coins
}

func NewMsgSendCoins(from sdk.AccAddress, to sdk.AccAddress, amount sdk.Coins) MsgSendCoins {
	return MsgSendCoins{
		From:   from,
		To:     to,
		Amount: amount,
	}
}

func (msg MsgSendCoins) Route() string { return "glimmer" }

func (msg MsgSendCoins) Type() string { return "send_coins" }

func (msg MsgSendCoins) ValidateBasic() sdk.Error {

	if msg.From.Empty() {
		return sdk.ErrInvalidAddress(msg.From.String())
	}

	if msg.To.Empty() {
		return sdk.ErrInvalidAddress(msg.To.String())
	}

	if !msg.Amount.IsPositive() {
		return sdk.ErrInsufficientCoins("Amount must be positive")
	}

	return nil
}

func (msg MsgSendCoins) GetSignBytes() []byte {
	b, err := json.Marshal(msg)
	if err != nil {
		panic(err)
	}
	return sdk.MustSortJSON(b)
}

func (msg MsgSendCoins) GetSigners() []sdk.AccAddress {
	return []sdk.AccAddress{msg.From}
}
