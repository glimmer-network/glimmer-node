package glimmer

import (
	"github.com/cosmos/cosmos-sdk/codec"
)

// RegisterCodec registers concrete types on the Amino codec
func RegisterCodec(cdc *codec.Codec) {
	cdc.RegisterConcrete(MsgSendCoins{}       , "glimmer/SendCoins"       , nil)
	cdc.RegisterConcrete(MsgAddFactDimension{}, "glimmer/AddFactDimension", nil)
	cdc.RegisterConcrete(MsgAddFact{}         , "glimmer/AddFact"         , nil)
}
