package glimmer

import (
	"encoding/json"
	"fmt"
	"github.com/cosmos/cosmos-sdk/codec"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/cosmos/cosmos-sdk/x/bank"
)

/***********************************************************************
Keepers: general
***********************************************************************/

type Keeper struct {
	coinKeeper         bank.Keeper
	factDimensionKey   sdk.StoreKey
	factStoreKey       sdk.StoreKey
	factByDimensionKey sdk.StoreKey
	cdc                *codec.Codec
}

func NewKeeper(coinKeeper bank.Keeper, factDimensionStoreKey sdk.StoreKey, factStoreKey sdk.StoreKey, factByDimensionKey sdk.StoreKey, cdc *codec.Codec) Keeper {
	return Keeper{
		coinKeeper            : coinKeeper,
		factDimensionKey      : factDimensionStoreKey,
		factStoreKey          : factStoreKey,
		factByDimensionKey    : factByDimensionKey,
		cdc                   : cdc,
	}
}

/***********************************************************************
Fact Keepers
***********************************************************************/

func (k Keeper) GetFact(ctx sdk.Context, address sdk.AccAddress) string {
	store := ctx.KVStore(k.factStoreKey)
	factAsBytes := store.Get(address)
	return string(factAsBytes)
}

func (k Keeper) SetFact(ctx sdk.Context, address sdk.AccAddress, toStore MsgAddFact) {
	store := ctx.KVStore(k.factStoreKey)
	b, err := json.Marshal(toStore)
	if err != nil {
		fmt.Println(err)
	}
	store.Set( toStore.Address, []byte(b))
}

/***********************************************************************
Fact Dimension Keepers
***********************************************************************/

func (k Keeper) GetFactDimension(ctx sdk.Context, name string) *string {
	store := ctx.KVStore(k.factDimensionKey)
	factDimensionAsBytes := store.Get([]byte(name))
	if factDimensionAsBytes == nil {
		return nil
	} else {
		factDimension := string(factDimensionAsBytes)
		return &factDimension
	}
}

func (k Keeper) SetFactDimension(ctx sdk.Context, toStore MsgAddFactDimension) {
	store := ctx.KVStore(k.factDimensionKey)
	b, err := json.Marshal(toStore)
	if err != nil {
		fmt.Println(err)
	}
	store.Set([]byte(toStore.Name), []byte(b))
}

/***********************************************************************
FactByDimension Keepers (look-up or set fact addresses for a dimension)
***********************************************************************/

// Given a factDimensionName returns nil if not found, or the contents, which should be
// a CSV of addresses "cosmos3jh23rh893...,cosmos213rh893f84r..."
func (k Keeper) GetFactAddressesByDimension (ctx sdk.Context, factDimensionName string) *string {
	store := ctx.KVStore(k.factByDimensionKey)
	resultsAsBytes := store.Get([]byte(factDimensionName))
	if resultsAsBytes == nil {
		return nil
	} else {
		factDimension := string(resultsAsBytes)
		return &factDimension
	}
}

// Associates a CSV of addresses "cosmos3jh23rh893...,cosmos213rh893f84r..." with a fact dimension name
func (k Keeper) SetFactAddressesByDimension (ctx sdk.Context, factDimensionName string, addressesAsCSVString string) {
	store := ctx.KVStore(k.factByDimensionKey)
	store.Set([]byte(factDimensionName), []byte(addressesAsCSVString))
}