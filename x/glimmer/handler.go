package glimmer

import (
	"fmt"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"strings"
)

/***********************************************************************s
	Handler: general
***********************************************************************/

func NewHandler(keeper Keeper) sdk.Handler {
	return func(ctx sdk.Context, msg sdk.Msg) sdk.Result {
		switch msg := msg.(type) {
		case MsgAddFact:
			return handleMsgAddFact(ctx, keeper, msg)
		case MsgAddFactDimension:
			return handleMsgAddFactDimension(ctx, keeper, msg)
		case MsgSendCoins:
			return handleMsgSendCoins(ctx, keeper, msg)
		default:
			errMsg := fmt.Sprintf("Unrecognized glimmer msg type: %v", msg.Type())
			return sdk.ErrUnknownRequest(errMsg).Result()
		}
	}
}

/***********************************************************************s
	Add Fact
***********************************************************************/

func handleMsgAddFact(ctx sdk.Context, keeper Keeper, msg MsgAddFact) sdk.Result {

	// Check that the fact dimension exists.
	fact := keeper.GetFactDimension(ctx, msg.Dimension)
	if fact == nil {
		return sdk.ErrUnknownRequest("Specified dimension does not exist").Result()
	}

	// Add the fact to the keepers
	keeper.SetFact(ctx, msg.Address, msg)

	// Add the fact address to the 'dimension name -> facts' lookup.
	factsOnThisDimension := keeper.GetFactAddressesByDimension(ctx, msg.Dimension)
	if factsOnThisDimension == nil || len (*factsOnThisDimension) == 0 {
		keeper.SetFactAddressesByDimension(ctx, msg.Dimension, string(msg.Address))
	} else {
		addressRemovedIfItWasPresent := strings.Replace( *factsOnThisDimension, string(msg.Address), "", -1 )
		strayLeadingTrailingCommasRemoved := strings.Trim( addressRemovedIfItWasPresent + "," + string(msg.Address), "," )
		strayDoubleCommasRemoved := strings.Replace(strayLeadingTrailingCommasRemoved, ",,", ",", -1)
		keeper.SetFactAddressesByDimension(ctx, msg.Dimension, strayDoubleCommasRemoved)
	}

	return sdk.Result{}
}

/***********************************************************************s
	Add Fact Dimension
***********************************************************************/

func handleMsgAddFactDimension(ctx sdk.Context, keeper Keeper, msg MsgAddFactDimension) sdk.Result {
	keeper.SetFactDimension(ctx, msg)
	return sdk.Result{}
}

/***********************************************************************
	Send coins
***********************************************************************/

func handleMsgSendCoins(ctx sdk.Context, keeper Keeper, msg MsgSendCoins) sdk.Result {
	_, err := keeper.coinKeeper.SendCoins(ctx, msg.From, msg.To, msg.Amount)
	if err != nil {
		return sdk.ErrInsufficientCoins("Sender does not have enough coins").Result()
	}
	return sdk.Result{}
}
