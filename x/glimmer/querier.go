package glimmer

import (
	"github.com/cosmos/cosmos-sdk/codec"
	sdk "github.com/cosmos/cosmos-sdk/types"
	abci "github.com/tendermint/tendermint/abci/types"
)

/***********************************************************************
	Queriers: general
***********************************************************************/

const (
	QueryFactDimension        = "fact-dimension"
	QueryFact                 = "fact"
	QueryFactsByDimensionName = "facts-by-dimension"
)

func NewQuerier(keeper Keeper) sdk.Querier {
	return func(ctx sdk.Context, path []string, req abci.RequestQuery) (res []byte, err sdk.Error) {
		switch path[0] {
		case QueryFactDimension:
			return queryFactDimensionByName(ctx, path[1:], req, keeper)
		case QueryFact:
			return queryFactByAddress(ctx, path[1:], req, keeper)
		case QueryFactsByDimensionName:
			return queryFactsByDimensionName(ctx, path[1:], req, keeper)
		default:
			return nil, sdk.ErrUnknownRequest("unknown glimmer query endpoint")
		}
	}
}

/***********************************************************************
	Get fact Dimensions
***********************************************************************/

func queryFactDimensionByName(ctx sdk.Context, path []string, req abci.RequestQuery, keeper Keeper) (res []byte, err sdk.Error) {
	dimensionName := path[0]
	dimensionJson := *keeper.GetFactDimension(ctx, dimensionName)
	bz, err2 := codec.MarshalJSONIndent(keeper.cdc, dimensionJson)
	if err2 != nil {
		panic("could not marshal result to JSON")
	}
	return bz, nil
}

/***********************************************************************
	Get individual facts
***********************************************************************/

func queryFactByAddress(ctx sdk.Context, path []string, req abci.RequestQuery, keeper Keeper) (res []byte, err sdk.Error) {
	factAddress := path[0]
	maybeAddress, err1 := sdk.AccAddressFromBech32(factAddress)
	if err1 != nil {
		panic("Address not found")
	}
	factJson := keeper.GetFact(ctx, maybeAddress)
	bz, err2 := codec.MarshalJSONIndent(keeper.cdc, factJson)
	if err2 != nil {
		panic("could not marshal result to JSON")
	}
	return bz, nil
}

/***********************************************************************
	Lookup facts for a given fact dimension
***********************************************************************/

func queryFactsByDimensionName (ctx sdk.Context, path []string, req abci.RequestQuery, keeper Keeper) (res []byte, err sdk.Error) {
	dimensionName := path[0]
	facts := keeper.GetFactAddressesByDimension(ctx, dimensionName)
	if facts == nil {
		bz, err2 := codec.MarshalJSONIndent(keeper.cdc, "[]")
		if err2 != nil {
			panic("could not marshal result to JSON")
		}
		return bz, nil
	} else {
		factsList := *facts
		bz, err2 := codec.MarshalJSONIndent(keeper.cdc, factsList)
		if err2 != nil {
			panic("could not marshal result to JSON")
		}
		return bz, nil
	}
}