cd ~
rm -rf /home/vagrant/.glimmer-cli
rm -rf /home/vagrant/.glimmer
glimmer init --chain-id glimmer-testnet
glimmer-cli keys add niki
glimmer-cli keys add ali
glimmer add-genesis-account $(glimmer-cli keys show niki -a) 1000ray
glimmer add-genesis-account $(glimmer-cli keys show ali -a) 1000ray
glimmer-cli config chain-id glimmer-testnet
glimmer-cli config output json
glimmer-cli config indent true
glimmer-cli config trust-node true
