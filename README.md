# Glimmer Node

Glimmer full node, including `glimmer` (daemon) and `glimmer-cli`.

Usage notes below (based on Cosmos SDK tutorial) - 

#### Initialize configuration files and genesis file
```glimmer init --chain-id glimmer-testnet```

#### Create a genesis key
```glimmer-cli keys add niki```

#### Create a genesis key
```glimmer-cli keys add ali```

#### Add both accounts, with coins to the genesis file
```
glimmer add-genesis-account $(glimmer-cli keys show niki -a) 1000ray
glimmer add-genesis-account $(glimmer-cli keys show ali -a) 1000ray
```

#### Configure your CLI to eliminate need for chain-id flag
```
glimmer-cli config chain-id glimmer-testnet
glimmer-cli config output json
glimmer-cli config indent true
glimmer-cli config trust-node true
```

#### Start daemon
```glimmer start```

#### Send coins
```glimmer-cli tx glimmer send-coins cosmos1s82hy49ll0rjjps2fj0kjxraktk7r29g2fv7ld 100ray --from niki```

### Query balances
```glimmer-cli query account <address>```